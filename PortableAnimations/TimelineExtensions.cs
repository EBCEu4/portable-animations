﻿using System;
using Windows.UI.Xaml.Media.Animation;

namespace PortableAnimations
{
    public static class TimelineExtensions
    {
        public static Timeline For<T>(this Timeline o, Action<T> action) where T : class
        {
            var obj = o as T;

            if (obj != null)
            {
                action(obj);
                return o;
            }

            return o;
        }

        public static T Cast<T>(this object o)
        {
            var isT = o is T;
            if (isT)
                return (T)o;

            return default(T);
        }
    }
}