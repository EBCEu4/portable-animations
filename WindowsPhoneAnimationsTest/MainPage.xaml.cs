﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using PortableAnimations;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace WindowsPhoneAnimationsTest
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            Loaded += async (sender, args) =>
            {
                await Rectangle.CompositeTransform()
                    .Animate(ct => ct.TranslateX)
                    .To(100)
                    .EasingFunction(new CircleEase { EasingMode = EasingMode.EaseIn })
                    .BeginAsync();

                Rectangle.Animate(r => r.Fill).To(Colors.Orange).Begin();

                Rectangle.CompositeTransform(.5, .5).Animate(b => b.Rotation).To(100).Begin();

                await Rectangle.CompositeTransform()
                    .Animate(ct => ct.TranslateX)
                    .To(0)
                    .Duration(3)
                    .EasingFunction(new CircleEase { EasingMode = EasingMode.EaseIn })
                    .BeginAsync();
            };
        }
    }
}
