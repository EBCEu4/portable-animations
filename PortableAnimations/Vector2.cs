﻿using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace PortableAnimations
{
    public struct Vector2
    {
        private Vector2(bool isEmpty)
            : this()
        {
            IsEmpty = isEmpty;
        }

        public Vector2(double x, double y)
            : this()
        {
            X = x;
            Y = y;
        }

        public static Vector2 Empty
        {
            get
            {
                return new Vector2(true);
            }
        }

        public bool IsEmpty { get; private set; }

        public double X { get; private set; }
        public double Y { get; private set; }

        public double Length
        {
            get { return System.Math.Sqrt(X * X + Y * Y); }
        }

        public Vector2 Normalize()
        {
            return new Vector2(X / Length, Y / Length);
        }

        public Vector2 LookAt(Vector2 v)
        {
            return v.Normalize() * Length;
        }

        public double DistanceTo(Vector2 v)
        {
            if (IsEmpty || v.IsEmpty)
            {
                double positiveInfinity = double.PositiveInfinity;
                return positiveInfinity;
            }

            return (this - v).Length;
        }

        public Vector2 Rotate(double angle)
        {
            var radians = angle.ToRadians();
            var cos = System.Math.Cos(radians);
            var sin = System.Math.Sin(radians);
            return new Vector2(X * cos - Y * sin, X * sin + Y * cos);
        }

        public double AngleTo(Vector2 v)
        {
            return (System.Math.Atan2(v.Y, v.X) - System.Math.Atan2(Y, X)).ToDegrees();
        }

        public static Vector2 operator +(CompositeTransform compositeTransform, Vector2 v)
        {
            compositeTransform.TranslateX += v.X;
            compositeTransform.TranslateY += v.Y;
            return compositeTransform.Position();
        }

        public static Vector2 operator +(UIElement uiElement, Vector2 v)
        {
            return uiElement.CompositeTransform() + v;
        }

        public static Vector2 operator -(CompositeTransform compositeTransform, Vector2 v)
        {
            compositeTransform.TranslateX -= v.X;
            compositeTransform.TranslateY -= v.Y;
            return compositeTransform.Position();
        }

        public static Vector2 operator -(UIElement uiElement, Vector2 v)
        {
            return uiElement.CompositeTransform() - v;
        }

        public static Vector2 operator *(Vector2 v, double d)
        {
            return new Vector2(v.X * d, v.Y * d);
        }

        public static Vector2 operator *(double d, Vector2 v)
        {
            return new Vector2(v.X * d, v.Y * d);
        }

        public static Vector2 operator +(Vector2 v, Point p)
        {
            return new Vector2(v.X + p.X, v.Y + p.Y);
        }

        public static Vector2 operator +(Point p, Vector2 v)
        {
            return new Vector2(v.X + p.X, v.Y + p.Y);
        }

        public static Vector2 operator -(Vector2 v, Point p)
        {
            return new Vector2(v.X - p.X, v.Y - p.Y);
        }

        public static Vector2 operator -(Point p, Vector2 v)
        {
            return new Vector2(p.X - v.X, p.Y - v.Y);
        }

        public static Vector2 operator +(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X + b.X, a.Y + b.Y);
        }

        public static Vector2 operator -(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X - b.X, a.Y - b.Y);
        }

        public static implicit operator Vector2(Point p)
        {
            return new Vector2(p.X, p.Y);
        }

        public static implicit operator Point(Vector2 v)
        {
            return new Point(v.X, v.Y);
        }
    }
}