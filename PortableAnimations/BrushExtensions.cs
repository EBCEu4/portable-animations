﻿using Windows.UI.Xaml.Media;

namespace PortableAnimations
{
    public static class BrushExtensions
    {
        public static SolidColorBrush AsSolidBrush(this Brush brush)
        {
            return brush as SolidColorBrush;
        }
    }
}