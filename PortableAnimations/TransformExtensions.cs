﻿using Windows.UI.Xaml.Media;

namespace PortableAnimations
{
    public static class TransformExtensions
    {
        public static Vector2 Position(this CompositeTransform compositeTransform)
        {
            return new Vector2(compositeTransform.TranslateX, compositeTransform.TranslateY);
        }

        public static Vector2 Position(this CompositeTransform compositeTransform, double x, double y)
        {
            compositeTransform.TranslateX = x;
            compositeTransform.TranslateY = y;

            return new Vector2(compositeTransform.TranslateX, compositeTransform.TranslateY);
        }

        public static Vector2 Scale(this CompositeTransform compositeTransform, double x, double y)
        {

            compositeTransform.ScaleX = x;
            compositeTransform.ScaleY = y;

            return new Vector2(compositeTransform.ScaleX, compositeTransform.ScaleY);
        }

        public static CompositeTransform ToCompositeTransform(this ScaleTransform scaleTransform)
        {
            var compositeTransform = new CompositeTransform();

            if (scaleTransform != null)
            {
                compositeTransform.CenterX = scaleTransform.CenterX;
                compositeTransform.CenterY = scaleTransform.CenterY;
                compositeTransform.ScaleX = scaleTransform.ScaleX;
                compositeTransform.ScaleY = scaleTransform.ScaleY;
            }

            return compositeTransform;
        }

        public static CompositeTransform ToCompositeTransform(this TranslateTransform translateTransform)
        {
            var compositeTransform = new CompositeTransform();

            if (translateTransform != null)
            {
                compositeTransform.TranslateX = translateTransform.X;
                compositeTransform.TranslateY = translateTransform.Y;
            }

            return compositeTransform;
        }


        public static CompositeTransform ToCompositeTransform(this RotateTransform rotateTransform)
        {
            var compositeTransform = new CompositeTransform();

            if (rotateTransform != null)
            {
                compositeTransform.CenterX = rotateTransform.CenterX;
                compositeTransform.CenterY = rotateTransform.CenterY;
                compositeTransform.Rotation = rotateTransform.Angle;
            }

            return compositeTransform;
        }

        public static CompositeTransform ToCompositeTransform(this SkewTransform skewTransform)
        {
            var compositeTransform = new CompositeTransform();

            if (skewTransform != null)
            {
                compositeTransform.CenterX = skewTransform.CenterX;
                compositeTransform.CenterY = skewTransform.CenterY;
                compositeTransform.SkewX = skewTransform.AngleX;
                compositeTransform.SkewY = skewTransform.AngleY;
            }

            return compositeTransform;
        }
    }
}