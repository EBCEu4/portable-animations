﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;

namespace PortableAnimations
{
    public static class AnimationUtils
    {
        public static Task<Storyboard> BeginAsync(this Storyboard storyboard)
        {
            EventHandler<object> storyboardOnCompleted = null;
            var source = new TaskCompletionSource<Storyboard>();

            storyboardOnCompleted = (sender, o) =>
            {
                storyboard.Completed -= storyboardOnCompleted;
                source.SetResult(storyboard);
            };
            storyboard.Completed += storyboardOnCompleted;
            storyboard.Begin();

            return source.Task;
        }

        public static Task<Storyboard> BeginAsync(this IAnimation animation)
        {
            return animation.Parent.BeginAsync();
        }

        public static Storyboard Begin(this IAnimation animation)
        {
            animation.Parent.Begin();
            return animation.Parent;
        }

        public static Animation<TElement, TProp> Animate<TElement, TProp>(this TElement element, Expression<Func<TElement, TProp>> expression) where TElement : DependencyObject
        {
            if (element == null)
                return Animation<TElement, TProp>.Empty;

            var propertyName = ((MemberExpression)expression.Body).Member.Name;

            return new Animation<TElement, TProp>(new Storyboard(), element, propertyName);
        }

        public static Animation<TElement, TProp> Animate<TElement, TProp>(this Animation<TElement, TProp> animation, Expression<Func<TElement, TProp>> expression) where TElement : DependencyObject
        {
            if (animation.IsEmpty)
                return animation;

            var propertyName = ((MemberExpression)expression.Body).Member.Name;

            return new Animation<TElement, TProp>(animation.Parent, animation.Target, propertyName);
        }


        //Brush animation
        public static Animation<SolidColorBrush, Color> Animate<TElement>(this TElement element, Expression<Func<TElement, Brush>> expression) where TElement : DependencyObject
        {
            var brush = expression.Compile()(element) as SolidColorBrush;
            if (brush == null)
            {
                var propertyName = ((MemberExpression)expression.Body).Member.Name;
                var solidColorBrush = new SolidColorBrush { Color = Colors.Transparent };
                element.GetType()
                    .GetRuntimeProperty(propertyName)
                    .SetValue(element, solidColorBrush);

                return new Animation<SolidColorBrush, Color>(new Storyboard(), solidColorBrush, "Color");
            }
            return new Animation<SolidColorBrush, Color>(new Storyboard(), brush, "Color");
        }
    }
}