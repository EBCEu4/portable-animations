using Windows.UI.Xaml.Media.Animation;

namespace PortableAnimations
{
    public interface IAnimation
    {
        Storyboard Parent { get; }
        bool IsEmpty { get; }
    }
}