using System;
using System.Linq.Expressions;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Animation;

namespace PortableAnimations
{
    public class Animation<TTarget, TProp> : IAnimation where TTarget : DependencyObject
    {
        public Storyboard Parent { get; private set; }

        public static Animation<TTarget, TProp> Empty
        {
            get { return new Animation<TTarget, TProp>(); }
        }

        public bool IsEmpty { get; private set; }

        private Animation()
        {
            IsEmpty = true;
            Parent = new Storyboard();
        }

        public Animation(Storyboard parent, TTarget target, string propertyName)
        {
            IsEmpty = false;
            Parent = parent;
            Target = target;

            Timeline animation = null;

            var type = typeof(TProp);

            if (type == typeof(double))
            {
                animation = new DoubleAnimation();
            }
            else if (type == typeof(Point))
            {
                animation = new PointAnimation();
            }
            else if (type == typeof(Color))
            {
                animation = new ColorAnimation();
            }

            Storyboard.SetTarget(animation, target);
            Storyboard.SetTargetProperty(animation, propertyName);
            parent.Children.Add(animation);
            _inner = animation;
        }

        private readonly Timeline _inner;

        public void SetValue(DependencyProperty dp, object value)
        {
            if (IsEmpty)
                return;

            _inner.SetValue(dp, value);
        }

        public void ClearValue(DependencyProperty dp)
        {
            if (IsEmpty)
                return;

            _inner.ClearValue(dp);
        }

        public object ReadLocalValue(DependencyProperty dp)
        {
            if (IsEmpty)
                return null;

            return _inner.ReadLocalValue(dp);
        }

        public object GetAnimationBaseValue(DependencyProperty dp)
        {
            if (IsEmpty)
                return null;

            return _inner.GetAnimationBaseValue(dp);
        }

        public TProp GetValue(DependencyProperty dp)
        {
            if (IsEmpty)
                return default(TProp);

            return (TProp)_inner.GetValue(dp);
        }

        public CoreDispatcher Dispatcher
        {
            get
            {
                if (IsEmpty)
                    return default(CoreDispatcher);

                return _inner.Dispatcher;
            }
        }

        public Animation<TTarget, TProp> FillBehavior(FillBehavior fillBehavior)
        {
            if (IsEmpty)
                return this;

            _inner.FillBehavior = fillBehavior;
            return this;
        }

        public Animation<TTarget, TProp> AllowDependentAnimations()
        {
            if (IsEmpty)
                return this;

            _inner
                .For<DoubleAnimation>(a => a.SetValue(DoubleAnimation.EnableDependentAnimationProperty, true))
                .For<PointAnimation>(a => a.SetValue(PointAnimation.EnableDependentAnimationProperty, true))
                .For<ColorAnimation>(a => a.SetValue(ColorAnimation.EnableDependentAnimationProperty, true));

            return this;
        }

        public Animation<TTarget, TProp> RepeatForever()
        {
            if (IsEmpty)
                return this;

            _inner.RepeatBehavior = RepeatBehavior.Forever;
            return this;
        }

        public Animation<TTarget, TProp> AutoReverse()
        {
            if (IsEmpty)
                return this;

            _inner.AutoReverse = true;
            return this;
        }

        public Animation<TTarget, TProp> Duration(double seconds)
        {
            if (IsEmpty)
                return this;

            _inner.Duration = TimeSpan.FromSeconds(seconds);
            return this;
        }

        public Animation<TTarget, TProp> Delay(double seconds)
        {
            if (IsEmpty)
                return this;

            _inner.BeginTime = TimeSpan.FromSeconds(seconds);
            return this;
        }

        public Animation<TTarget, TProp> SpeedRatio(double speedRatio)
        {
            if (IsEmpty)
                return this;

            _inner.SpeedRatio = speedRatio;
            return this;
        }


        public Animation<TTarget, TProp> EasingFunction(EasingFunctionBase function)
        {
            if (IsEmpty)
                return this;

            _inner
                .For<DoubleAnimation>(a => a.EasingFunction = function)
                .For<PointAnimation>(a => a.EasingFunction = function)
                .For<ColorAnimation>(a => a.EasingFunction = function);

            return this;
        }


        public Animation<TTarget, TProp> From(TProp value)
        {
            if (IsEmpty)
                return this;

            _inner
                   .For<DoubleAnimation>(a => a.From = value.Cast<double?>())
                   .For<PointAnimation>(a => a.From = value.Cast<Point?>())
                   .For<ColorAnimation>(a => a.From = value.Cast<Color?>());

            return this;
        }

        public Animation<TTarget, TProp> To(TProp value)
        {
            if (IsEmpty)
                return this;

            _inner
                .For<DoubleAnimation>(a => a.To = value.Cast<double?>())
                .For<PointAnimation>(a => a.To = value.Cast<Point?>())
                .For<ColorAnimation>(a => a.To = value.Cast<Color?>());

            return this;
        }

        public Animation<TTarget, TProp> By(TProp value)
        {
            if (IsEmpty)
                return this;

            _inner
                   .For<DoubleAnimation>(a => a.By = value.Cast<double?>())
                   .For<PointAnimation>(a => a.By = value.Cast<Point?>())
                   .For<ColorAnimation>(a => a.By = value.Cast<Color?>());

            return this;
        }

        public TTarget Target { get; private set; }

        public static implicit operator Timeline(Animation<TTarget, TProp> animation)
        {
            if (animation.IsEmpty)
                return new DoubleAnimation();

            return animation._inner;
        }


    }
}