﻿using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace PortableAnimations
{
    public static class UiElementExtensions
    {
        public static CompositeTransform CompositeTransform(this UIElement element, double? transformOriginX = null,
            double? transformOriginY = null)
        {
            var x = element.RenderTransformOrigin.X;
            var y = element.RenderTransformOrigin.Y;

            if (transformOriginX != null)
                x = transformOriginX.Value;

            if (transformOriginX != null)
                y = transformOriginX.Value;

            if (transformOriginX != null || transformOriginY != null)
                element.RenderTransformOrigin = new Point(x, y);

            var transform = CreateCompositeTransform(element.RenderTransform);
            element.RenderTransform = transform;
            return transform;
        }

        public static CompositeTransform CreateCompositeTransform(Transform initialTransform)
        {
            CompositeTransform transform;
            if (initialTransform == null)
            {
                transform = new CompositeTransform();
                return transform;
            }

            transform = initialTransform as CompositeTransform;
            if (transform != null)
                return transform;


            if (initialTransform is MatrixTransform)
            {
                transform = new CompositeTransform();
            }
            else if (initialTransform is TranslateTransform)
            {
                transform = ((TranslateTransform)initialTransform).ToCompositeTransform();
            }
            else if (initialTransform is RotateTransform)
            {
                transform = ((RotateTransform)initialTransform).ToCompositeTransform();
            }

            else if (initialTransform is ScaleTransform)
            {
                transform = ((ScaleTransform)initialTransform).ToCompositeTransform();
            }
            else if (initialTransform is SkewTransform)
            {
                transform = ((SkewTransform)initialTransform).ToCompositeTransform();
            }

            return transform;
        }
    }
}