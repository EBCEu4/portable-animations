﻿namespace PortableAnimations
{
    public static class Math
    {
        public static double ToRadians(this double angle)
        {
            return angle * (System.Math.PI / 180d);
        }

        public static double ToDegrees(this double radians)
        {
            return radians * (180d / System.Math.PI);
        }

        public static bool IsInRange(this double value, double min, double max)
        {
            return value >= min && value <= max;
        }
    }
}