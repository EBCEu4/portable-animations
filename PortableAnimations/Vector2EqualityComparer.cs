using System.Collections.Generic;

namespace PortableAnimations
{
    public class Vector2EqualityComparer : IEqualityComparer<Vector2>
    {
        private double _minDistance;

        public Vector2EqualityComparer(double minDistance)
        {
            _minDistance = minDistance;
        }

        public bool Equals(Vector2 x, Vector2 y)
        {
            var @equals = System.Math.Abs((x - y).Length) < _minDistance;
            return @equals;
        }

        public int GetHashCode(Vector2 v)
        {
            return v.GetHashCode();
        }
    }
}